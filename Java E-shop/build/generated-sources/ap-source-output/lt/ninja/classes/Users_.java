package lt.ninja.classes;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Users.class)
public abstract class Users_ {

	public static volatile SingularAttribute<Users, String> password;
	public static volatile SingularAttribute<Users, String> salt;
	public static volatile SingularAttribute<Users, Integer> id;
	public static volatile SingularAttribute<Users, Short> isAdmin;
	public static volatile SingularAttribute<Users, String> userName;
	public static volatile ListAttribute<Users, Cart> cartList;

	public static final String PASSWORD = "password";
	public static final String SALT = "salt";
	public static final String ID = "id";
	public static final String IS_ADMIN = "isAdmin";
	public static final String USER_NAME = "userName";
	public static final String CART_LIST = "cartList";

}

