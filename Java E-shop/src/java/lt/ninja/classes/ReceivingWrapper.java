/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ninja.classes;

import java.util.Date;

/**
 *
 * @author Lukas
 */
public class ReceivingWrapper {

    private Integer goodId;
    private Integer quantity;
    private Date receivingDate;
    private String receivingInfo;

    public ReceivingWrapper() {
    }

    public ReceivingWrapper(Integer goodId, Integer quantity, Date receivingDate, String receivingInfo) {
        this.goodId = goodId;
        this.quantity = quantity;
        this.receivingDate = receivingDate;
        this.receivingInfo = receivingInfo;
    }

    public Integer getGoodId() {
        return goodId;
    }

    public void setGoodId(Integer goodId) {
        this.goodId = goodId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Date getReceivingDate() {
        return receivingDate;
    }

    public void setReceivingDate(Date receivingDate) {
        this.receivingDate = receivingDate;
    }

    public String getReceivingInfo() {
        return receivingInfo;
    }

    public void setReceivingInfo(String receivingInfo) {
        this.receivingInfo = receivingInfo;
    }

    @Override
    public String toString() {
        return "ReceivingWrapper{" + "goodId=" + goodId + ", quantity=" + quantity + ", receivingDate=" + receivingDate + ", receivingInfo=" + receivingInfo + '}';
    }

}
