/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
//import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Lukas
 */
@Entity
@Table(name = "people")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "People.findAll", query = "SELECT p FROM People p")
    , @NamedQuery(name = "People.findById", query = "SELECT p FROM People p WHERE p.id = :id")
    , @NamedQuery(name = "People.findByFirstName", query = "SELECT p FROM People p WHERE p.firstName = :firstName")
    , @NamedQuery(name = "People.findByLastName", query = "SELECT p FROM People p WHERE p.lastName = :lastName")
    , @NamedQuery(name = "People.findByBirthDate", query = "SELECT p FROM People p WHERE p.birthDate = :birthDate")
    , @NamedQuery(name = "People.findBySalary", query = "SELECT p FROM People p WHERE p.salary = :salary")})
public class People implements Serializable {

    private static final long serialVersionUID = 1L;
   
    private Integer id;
  
    private String firstName;
   
    private String lastName;
   
    private Date birthDate;
   
    private BigDecimal salary;
   
    private List<Addresses> addressesList;
   
    private List<Contacts> contactsList;

    public People() {
    }

    public People(Integer id) {
        this.id = id;
    }

    public People(Integer id, String firstName, String lastName, Date birthDate, BigDecimal salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.salary = salary;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    @Basic(optional = false)
    @NotNull
    @Column(name = "birth_date")
    @Temporal(TemporalType.DATE)
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
         if(birthDate != null){
        this.birthDate = new Date(birthDate.getTime());}
        else{
            this.birthDate=null;}
    }
 // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "salary")
    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    @XmlTransient
   // @JsonbTransient
    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "peopleId")
    public List<Addresses> getAddressesList() {
        return addressesList;
    }

    public void setAddressesList(List<Addresses> addressesList) {
        this.addressesList = addressesList;
    }

    @XmlTransient
  //  @JsonbTransient
    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "peopleId")
    public List<Contacts> getContactsList() {
        return contactsList;
    }

    public void setContactsList(List<Contacts> contactsList) {
        this.contactsList = contactsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof People)) {
            return false;
        }
        People other = (People) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "People[ id=" + id + " ]";
    }
    
}
